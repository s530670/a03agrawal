var path = require("path");
var moment = require("moment");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");
var app = express();  // make express app

var api_key = 'key-58373f910b52cb87142f833f3b646658';
var domain = 'sandbox2f6c24e87b0f4ad6aa021d50908cd8cb.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

var server = require('http').createServer(app); // inject app into the server

//public folder
app.use(express.static(path.join(__dirname, 'assets/')));

// 1 set up the view engine
app.set("views", path.resolve(__dirname, 'assets/')); // path to views
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// 2 create an array to manage our entries
var entries = [];
app.locals.entries = entries; // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"));     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }));


// 4 handle http GET requests 
app.get("/", function (request, response) {
  response.render('index.html');
});
app.get("/about", function (request, response) {
  response.render('views/about-me.html');
});
app.get("/contact", function (request, response) {
  response.render('views/contact-us.html');
});
app.get("/guestbook", function (request, response) {
  response.render('views/guestbook.html',{entries : entries});
});
app.get("/extras", function (request, response) {
  response.render('views/extras.html');
});
app.get("/new-entry", function (request, response) {
  response.render('views/new-entry.html');
});

// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: moment().format("ddd MMM Do YYYY, HH:mm")
  });
  response.redirect("/guestbook");  // where to go next? Let's go to the home page :)
});

app.post('/contactUs',function(req,res){
  var data = {
    from: 'Abhi<abhijitagrawal09@gmail.com>',
    to: req.body.email,
    subject: 'Confirmation of Query Submission',
    text: 'Hi '+ req.body.name +' ,Thanks for contacting us. Will get back to you within 24 hrs.'
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    res.send({status:400 , message:"Thanks for contacting us"});
  });
  
})

// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  response.status(404).render("404.html");
});

// Listen for an application request on port 8081 & notify the developer
server.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});
