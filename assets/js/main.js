$("#formMsg").hide();

function submitForm() {
        var name = document.getElementById("name").value;
        var email = document.getElementById("email").value;
        var query = document.getElementById("query").value;
        
        $.post("/contactUs",
        {
            "name": name,
            "email": email,
            "query" : query
        },
        function(data, status){
                resetInputs();
                $("#formMsg").show();
        });
        
        
}

//Resets the form
function resetInputs() {
        $("#name").val("");
        $("#email").val("");
        $("#query").val("");
}